/*
 * main.cpp
 *
 *  Created on: May 21, 2014
 *      Author: geniusye
 */

#include <stdio.h>
#include <stdlib.h>
#include <setjmp.h>
#include <string.h>

static jmp_buf env;

int jump_fun(int k);
void printkk(int k);

int main(int argc, char **argv, char **environ)
{
	char **ep;
	for (ep = environ; *ep != NULL; ep++)
	{
	//	puts(*ep);
	}
	char *a = getenv("SHELL");
	printf("%s", a);

	int i = setjmp(env);
	if (i == 0)
		jump_fun(0);

	char *s = (char *)alloca(10);
	sprintf(s, "abcdefghi");
	*(s + 9) = 'j';

	printkk(1);

	*(s + 14) = 0;

	for (int i = 0; i < 40; i += 8)
		printf("%p %d %d %d %d %d %d %d %d\n ", s + i, *(s + i), *(s + i + 1), *(s + i + 2), *(s + i + 3), *(s + i + 4), *(s + i + 5), *(s + i + 6), *(s + i + 7));

	printf("\n");

	return 0;
}

int jump_fun(int k)
{
	printf("current level = %d\n", k);
	if (k > 10)
		longjmp(env, 1);
	else
		jump_fun(k+1);
	return k;
}

void printkk(int k)
{
	printf("k in fun is %d\n", k);
}


