//============================================================================
// Name        : cpp_p_ch13.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <utility>
#include <vector>
#include <string.h>

class Foo
{
public:
	Foo()
	{
		pi = new int[100];
		std::cout << "Constructor" << std::endl;
	};
	~Foo()
	{
		delete [] pi;
	}
	Foo(const Foo &f)
	{
		pi = new int[100];
		memmove(pi, f.pi, sizeof(int) * 100);
		std::cout << "Copy constructor" << std::endl;
	}
	Foo (Foo &&f) noexcept
	{
		delete [] pi;
		pi = f.pi;
		f.pi = NULL;
		std::cout << "Move constructor" << std::endl;
	}
	Foo &operator= (const Foo &f)
	{
		if (this != &f)
		{
			memmove(pi, f.pi, sizeof(int) * 100);
		}
		std::cout << "Copy Assignment Operator" << std::endl;
		return *this;
	}
	Foo &operator= (Foo &&f) noexcept
	{
		if (this != &f)
		{
			delete [] pi;
			pi = f.pi;
			f.pi = NULL;
		}
		std::cout << "Move Assignment Operator" << std::endl;
		return *this;
	}
	int *pi;
};


Foo generateFoo()
{
	Foo o;
	*o.pi = 2;
	return o;
}

void fun(Foo f) { std::cout << *f.pi << std::endl; }

void fun_l_refer(Foo &f) { std::cout << *f.pi << std::endl; }

void fun_r_refer(Foo &&f) { std::cout << *f.pi << std::endl; }

void int_r(int &&i) { std::cout << "int_r ouput : " << i << std::endl; }

void forward_print (int i, int &j)
{
	std::cout << "Forward_print : i = " << i << " j = " << ++j;
}

void forward_print_2 (int &&i, int &j)
{
	std::cout << "Forward_print : i = " << i << " j = " << ++j;
}

template<typename T1, typename T2, typename T3>
void forward_clip (T1 f, T2 p1, T3 p2)
{
	f(p1, p2);
}

template<typename T1, typename T2, typename T3>
void forward_clip_2 (T1 f, T2 &&p1, T3 &&p2)
{
	f(std::forward<T2>(p1), std::forward<T3>(p2));
}

int main() {
	std::cout << "Foo o" << std::endl;
	Foo o;
	std::cout << "Foo o2 = o" << std::endl;
	Foo o2 = o;
	std::cout << "Foo o3; o3 = o;" << std::endl;
	Foo o3;
	o3 = o;
	std::cout << "Foo o5 = generateFoo();" << std::endl;
	Foo o5 = generateFoo();
	std::cout << "Foo o6; o6 = generateFoo();" << std::endl;
	Foo o6;
	o6 = generateFoo();
	std::cout << "Foo &&o7 = Foo();" << std::endl;
	Foo &&o7 = Foo();
	*o7.pi = 2;

	std::vector<Foo> f;
	f.reserve(10);
	std::cout << "f.push_back(Foo());" << std::endl;
	f.push_back(Foo());
	std::cout << "Foo ov1; f.push_back(ov1);" << std::endl;
	Foo ov1;
	f.push_back(ov1);
	std::cout << "f.push_back(generateFoo());" << std::endl;
	f.push_back(generateFoo());

	std::cout << "Foo o5_ = generateFoo(); fun(o5_);" << std::endl;
	Foo o5_ = generateFoo();
	fun(o5_);
	std::cout << "Foo o5_l = generateFoo(); fun_l_refer(o5_l);" << std::endl;
	Foo o5_l = generateFoo();
	fun_l_refer(o5_l);
	std::cout << "fun_r_refer(generateFoo());" << std::endl;
	fun_r_refer(generateFoo());

	int_r(1);
	int i = 2;
	int_r(i * 4);
	int_r(std::move(i));
	fun_r_refer(std::move(o5_l));

	int j = 3;
	forward_clip(forward_print, i, j);
	std::cout << " outside j = " << j << " should = 4" << std::endl;
	forward_clip_2(forward_print, i, j);
	std::cout << " outside j = " << j << " should = 4" << std::endl;
	forward_clip_2(forward_print_2, 10, j);
	std::cout << " outside j = " << j << " should = 5" << std::endl;

	return 0;
}
