/*
 * main.cpp
 *
 *  Created on: Aug 19, 2014
 *      Author: geniusye
 */

#include <stdio.h>
#include <sys/socket.h>
#include <unistd.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <arpa/inet.h>
#include <sys/epoll.h>

#define MAXLINE 1024

void error(const char *error)
{
	printf("%s\n", error);
	exit(0);
}

int main()
{
	pid_t pid = fork();
	if (pid != 0)
	{
		int r;
		int fd;
		fd = socket(AF_INET6, SOCK_DGRAM, 0);
		if (fd < 0)
			error("socket error");

		struct sockaddr_in6 address;
		memset(&address, 0, sizeof(struct sockaddr_in6));
		address.sin6_family = AF_INET6;
		address.sin6_port = htons(9000);
		address.sin6_addr = in6addr_any;

		r = bind(fd, (struct sockaddr *)&address, sizeof(struct sockaddr_in6));
		if (r < 0)
			error("bind error");

		int epoll_fd = epoll_create(1);
		if (epoll_fd < 0)
			error("epoll_create error");

		struct epoll_event ev;
		ev.events = EPOLLIN;

		r = epoll_ctl(epoll_fd, EPOLL_CTL_ADD, fd, &ev);
		if (r < 0)
			error("epoll_ctl add error");

		char *buffer = (char *)malloc(MAXLINE);
		struct epoll_event list[1];
		while (1)
		{
			int count = epoll_wait(epoll_fd, list, 1, 0);
			if (count < 0)
				error("epoll_wait error");

			int s = recvfrom(fd, buffer, MAXLINE, 0, NULL, NULL);
			if (s < 0)
				error("recv error");

			if (!strcmp(buffer, "quit"))
			{
				printf("Receive Quit Instruction\n");
				break;
			}

			printf("Receive data from client, len = %d, data:%s\n", s, buffer);
		}
		free(buffer);
	}
	else
	{
		sleep(1);
		int fd;
		fd = socket(AF_INET6, SOCK_DGRAM, 0);
		if (fd < 0)
			error("socket error");

		struct sockaddr_in6 address;
		memset(&address, 0, sizeof(struct sockaddr_in6));
		address.sin6_family = AF_INET6;
		address.sin6_addr = in6addr_loopback;
		address.sin6_port = htons(9000);

		char *buffer = (char *)malloc(MAXLINE);
		while (1)
		{
			printf("Type in command, type in quit to quit\n");
			scanf("%s", buffer);

			int len = strlen(buffer) + 1;
			char *start = buffer;
			do
			{
				int s = sendto(fd, start, len, 0, (struct sockaddr *)&address, sizeof(struct sockaddr_in6));
				if (s < 0)
					error("send error");
				else if (s < len)
				{
					start += s;
					len -= s;
				}
				else
					break;
			}
			while (1);

			if (!strcmp(buffer, "quit"))
				break;
		}
		free(buffer);
	}
}


