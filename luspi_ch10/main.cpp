/*
 * main.cpp
 *
 *  Created on: May 21, 2014
 *      Author: geniusye
 */

#include <time.h>
#include <sys/time.h>
#include <sys/times.h>
#include <stdio.h>

int main()
{
//	struct timeval tv;
//
//	gettimeofday(&tv, NULL);
//
//	printf("timeval .sec : %ld, .usec : %ld\n", tv.tv_sec, tv.tv_usec);
//
//	time_t timestamp = time(NULL);
//
//	printf("timestamp : %ld\n", timestamp);
//
//	struct tm time_gm = *gmtime(&timestamp);
//
//	struct tm time_local = *localtime(&timestamp);
//
//	printf("ctime : %s\n", ctime(&timestamp));
//
//	printf("asctime localtime %s\n", asctime(&time_local));
//
//	printf("asctime gmtime %s\n", asctime(&time_gm));
//
//	time_t tt = mktime(&time_gm);
//
//	printf("ctime mktime %s\n", ctime(&tt));

	time_t timestamp = time(NULL);

	struct tm time_gm = *gmtime(&timestamp);

	struct tm time_local = *localtime(&timestamp);

	time_t time2 = mktime(&time_local);

	printf("ctime local : %s", ctime(&time2));

	printf("asctime local : %s", asctime(&time_local));
	printf("asctime gm : %s", asctime(&time_gm));

	char a[100];

//	printf("%s\n", strftime(a, 100, "%Y %m %d %H/%I %M %S", &time_gm));

	struct tms buf;

	int i = 0, j = 0;
	while (i < 1000000) { while (j < 1000000) j++; i++; }

	clock_t clo = times(&buf);

	printf("%d\n", CLOCKS_PER_SEC);

	printf("%d %d %d %d\n", buf.tms_utime, buf.tms_stime, buf.tms_cutime, buf.tms_cstime);

	return 0;
}


