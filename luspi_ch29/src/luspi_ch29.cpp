//============================================================================
// Name        : luspi_ch29.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <vector>

int count = 0;

pthread_mutex_t mutex;
pthread_once_t once = PTHREAD_ONCE_INIT;
pthread_key_t key;

__thread int localvariable = 10;

void error_report(const char *error);

void destruct(void *) { }

void threadOnce(void)
{
	count = 100;
}

void *threadFun(void *arg)
{
	int r;
	r = pthread_once(&once, threadOnce);
	if (r != 0)
		error_report("thread once error");
	r = pthread_key_create(&key, destruct);
	if (r != 0)
		error_report("thread key create error");

	int *p = (int *)malloc(sizeof(int));

	pthread_setspecific(key, (void *)p);

	void *s = pthread_getspecific(key);

	printf("position of p & s = %p %p\n", p, s);

	printf("address of localvariable of int is %p\n", &localvariable);

	for (*p = 0; *p < 1000000; (*p)++)
	{
		r = pthread_mutex_lock(&mutex);
		if (r != 0)
			error_report("lock error");
		r = pthread_mutex_lock(&mutex);
		if (r != 0)
			error_report("lock error");
		count++;
		r = pthread_mutex_unlock(&mutex);
		if (r != 0)
			error_report("unlock error");
		r = pthread_mutex_unlock(&mutex);
		if (r != 0)
			error_report("unlock error");
	}
	return NULL;
}

int main()
{
	std::vector<pthread_t> pid_list;
	int r;

	printf("address of localvariable of int is %p\n", &localvariable);

	pthread_mutexattr_t attr;
	pthread_mutexattr_init(&attr);
	pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE);

	pthread_mutex_init(&mutex, &attr);

	for (int i = 0; i < 10; i++)
	{
		pthread_t pid;
		r = pthread_create(&pid, NULL, threadFun, NULL);
		if (r < 0)
			error_report("pthread_create error");
		pid_list.push_back(pid);
	}

	for (int i = 0; i < 10; i++)
	{
		r = pthread_join(pid_list[i], NULL);
		if (r < 0)
			error_report("pthread_join error");
	}

	pthread_mutex_destroy(&mutex);

	printf("count = %d\n", count);
}

void error_report(const char *error)
{
	printf("%s\n", error);
	exit(1);
}
