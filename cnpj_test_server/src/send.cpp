/*
 * send.cpp
 *
 *  Created on: May 18, 2014
 *      Author: geniusye
 */

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <unistd.h>
#include <map>

#include "process.h"
#include "client.h"

void send(int num_msg);
void send_to_same_user(int num_msg);

void *
send_thread_main(void *par)
{
	int *place = (int *)par;
	int num1 = *place;
	int num2 = *(place + 1);
	free(par);

	send(num1);
	send_to_same_user(num2);
	return NULL;
}

void
send(int number_msg)
{
	int num = NUM_CLIENT + NUM_CHANNEL;
	srand(time(0));
	for (int i = 0; i < number_msg; i++)
	{
		char a[10];
		sprintf(a, "%d", i);
		std::string msg = "This is a messages. message id is " + std::string(a) + "\n";

		int src = rand() % NUM_CLIENT;
		client *curr_c = clients[src];

		src = rand() % num;
		if (src < NUM_CLIENT)
		{
			// send to client
			int dest = src;
			while (dest == src)
			{
				dest = rand() % NUM_CLIENT;
			}
			client *dest_c = clients[dest];

			dest_c->add_should_recvmsg(":" + curr_c->get_nick() + " PRIVMSG " + dest_c->get_nick() + " :" + msg);
			curr_c->add_command("PRIVMSG " + dest_c->get_nick() + " :" + msg);
		}
		else
		{
			// send to channel
			src -= NUM_CLIENT;
			std::string &channel = channels[src];

			curr_c->add_command("PRIVMSG " + channel + " :" + msg);

			typedef std::multimap<std::string, client *>::const_iterator cir;
			std::pair<cir, cir> range = channel_to_clients.equal_range(channel);

			for (cir iter = range.first; iter != range.second; ++iter)
			{
				i++;
				client *dest_c = iter->second;
				dest_c->add_should_recvmsg(":" + curr_c->get_nick() + " PRIVMSG " + channel + " :" + msg);
			}
			i--;
		}
		usleep(1000);
	}
}

void
send_to_same_user(int number_msg)
{
	srand(time(0));
	int dest = 0;
	client *dest_c = clients[dest];
	for (int i = 0; i < number_msg; i++)
	{
		int src = 0;
		while (src == 0)
		{
			src = rand() % NUM_CLIENT;
		}
		client *curr_c = clients[src];

		char a[100];
		sprintf(a, "%d", i);
		std::string msg = "This is a messages. message id is " + std::string(a) + "\n";
		dest_c->add_should_recvmsg(":" + curr_c->get_nick() + " PRIVMSG " + dest_c->get_nick() + " :" + msg);
		curr_c->add_command("PRIVMSG " + dest_c->get_nick() + " :" + msg);

		usleep(1000);
	}
}


