/*
 * Server.h
 *
 *  Created on: Jun 9, 2014
 *      Author: geniusye
 */

#ifndef SERVER_H_
#define SERVER_H_

class server
{
private:
	int port;
public:
	server(int port) : port(port) { }
	int get_port() { return this->port; }
};

#endif /* SERVER_H_ */
