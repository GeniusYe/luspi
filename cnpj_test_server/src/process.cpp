/*
 * process.cpp
 *
 *  Created on: May 18, 2014
 *      Author: geniusye
 */

#include <iostream>
#include <map>
#include <string>
#include <vector>

#include "process.h"

int epoll_fd;

std::multimap<std::string, client *> channel_to_clients;

std::vector<server *> servers;

std::vector<client *> clients;
std::vector<std::string> channels;

std::map<int, client *> fd_to_client;

void
panic(const std::string &error)
{
	std::cout << error << std::endl;
	exit(0);
}


