/*
 * compare.cpp
 *
 *  Created on: May 18, 2014
 *      Author: geniusye
 */

#include <iostream>
#include <vector>
#include <algorithm>
#include <string.h>

#include "process.h"
#include "client.h"

#define MAXLENGTH 1024

int
compare_msg(const std::string &str1, const std::string &str2)
{
	char c_str1[MAXLENGTH], c_str2[MAXLENGTH];
	int i = 0;
	for (std::string::const_iterator iter = str1.begin(); iter != str1.end(); ++iter)
		if (*iter != ' ' && *iter != '\n')
		{
			c_str1[i] = *iter;
			++i;
		}
	c_str1[i] = '\0';
	i = 0;
	for (std::string::const_iterator iter = str2.begin(); iter != str2.end(); ++iter)
		if (*iter != ' ' && *iter != '\n')
		{
			c_str2[i] = *iter;
			++i;
		}
	c_str2[i] = '\0';
	int r = strcmp(c_str1, c_str2);
	return r;
}

void
compare(int &missing_dst, int &duplicate_dst)
{
	int missing = 0, duplicate = 0;
	for (size_t i = 0; i < clients.size(); i++)
	{
		client *curr_c = clients[i];

		curr_c->sort_msg();
		std::vector<std::string>::const_iterator ptr1 = curr_c->should_recvmsg_begin();
		std::vector<std::string>::const_iterator ptr2 = curr_c->recvmsg_begin();

		while (ptr1 != curr_c->should_recvmsg_end() && ptr2 != curr_c->recvmsg_end())
		{
			int result = compare_msg(*ptr1, *ptr2);
			if (result == 0)
			{
				++ptr1;
				++ptr2;
			}
			else if (result > 0)
			{
				//std::cout << "duplicate:" << *ptr2 << std::endl;
				++ptr2;
				++duplicate;
			}
			else
			{
				//std::cout << "missing:" << *ptr1 << std::endl;
				++ptr1;
				++missing;
			}
		}
		while (ptr1 != curr_c->should_recvmsg_end())
		{
			++missing;
			++ptr1;
		}
		while (ptr2 != curr_c->recvmsg_end())
		{
			++duplicate;
			++ptr2;
		}
	}
	missing_dst = missing;
	duplicate_dst = duplicate;
}


