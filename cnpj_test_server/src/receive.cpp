/*
 * receive.cpp
 *
 *  Created on: May 18, 2014
 *      Author: geniusye
 */

#include <iostream>
#include <sstream>
#include <string>
#include <map>
#include <sys/epoll.h>
#include <sys/socket.h>
#include <errno.h>
#include "process.h"

static char buffer[BUFFER_SIZE];

bool if_record = false;				//mark whether we should record messages now

void
receive(int epoll_fd)
{
	struct epoll_event evlist[NUM_CLIENT];

	while(true)
	{
		int result = epoll_wait(epoll_fd, evlist, clients.size(), 2000);
		if (result < 0)
		{
			if (errno == EINTR)
				continue;
			else
				panic("epoll_wait error");
		}
		else if (result > 0)
		{
			for (int i = 0; i < result; i++)
			{
				struct epoll_event *curr_ev = &evlist[i];
				client *curr_cli = (client *)curr_ev->data.ptr;
				int fd = curr_cli->get_fd();
				while(true)
				{
					int len = recv(fd, buffer, BUFFER_SIZE, MSG_DONTWAIT);
					if (len < 0)
					{
						if (errno != EAGAIN)
							panic("receive error");
						else
							break;
					}
					else if (len == 0)
						panic("remote connection closed");

					if (if_record)
					{
						std::string temp_msg = std::string(buffer, len);
						//std::cout << temp_msg << std::endl;
						std::istringstream msg(temp_msg);
						std::string line;
						while(getline(msg, line))
							curr_cli->add_recvmsg(line);
					}
				}
			}
		}
		else
			break;
	}
}


