/*
 * client.cpp
 *
 *  Created on: May 18, 2014
 *      Author: geniusye
 */

#include <sstream>
#include <sys/socket.h>
#include <algorithm>
#include "client.h"

client::client(int fd) : fd(fd) { }

client::~client() { }

void
client::add_command(const std::string &command)
{
	send(this->fd, command.c_str(), command.size(), 0);
}

void
client::set_nick(const std::string &nick)
{
	this->nick = nick;
}

const std::string &
client::get_nick() const
{
	return this->nick;
}

int
client::get_fd() const
{
	return this->fd;
}

void
client::add_recvmsg(const std::string &msg)
{
	this->recvmsg.push_back(msg);
}
void
client::add_should_recvmsg(const std::string &msg)
{
	this->should_recvmsg.push_back(msg);
}

std::vector<std::string>::const_iterator
client::recvmsg_begin() const
{
	return this->recvmsg.begin();
}

std::vector<std::string>::const_iterator
client::recvmsg_end() const
{
	return this->recvmsg.end();
}

std::vector<std::string>::const_iterator
client::should_recvmsg_begin() const
{
	return this->should_recvmsg.begin();
}

std::vector<std::string>::const_iterator
client::should_recvmsg_end() const
{
	return this->should_recvmsg.end();
}

void
client::sort_msg()
{
	sort(this->should_recvmsg.begin(), this->should_recvmsg.end());
	sort(this->recvmsg.begin(), this->recvmsg.end());
}
