/*
 * process.h
 *
 *  Created on: May 18, 2014
 *      Author: geniusye
 */

#ifndef PROCESS_H_
#define PROCESS_H_

#include <stdlib.h>
#include <string>
#include <vector>
#include <map>
#include "client.h"
#include "server.h"

#define NUM_CLIENT 50
#define BUFFER_SIZE 2048
#define NUM_MESSAGE 5000
#define NUM_MESSAGE_SAME 1000
#define NUM_MESSAGE_AFTER_CHANGE 3000
#define NUM_CHANNEL 20
#define MAX_SERVER 20

extern std::vector<server *> servers;
extern std::multimap<std::string, client *> channel_to_clients;

extern int epoll_fd;
extern std::vector<client *> clients;
extern std::map<int, client *> fd_to_client;
extern std::vector<std::string> channels;

extern bool if_record;

void panic(const std::string &error);

void init();

void change_channel();

void clear_msg();

void *send_thread_main(void *);

void receive(int epoll_fd);

void compare(int &missing, int &duplicate);

#endif /* PROCESS_H_ */
