/*
 * init.cpp
 *
 *  Created on: May 18, 2014
 *      Author: geniusye
 */

#include <sys/epoll.h>
#include <sys/socket.h>
#include <sys/epoll.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sstream>
#include <string.h>
#include "process.h"

client *generate_client(int port);

void
init()
{
	struct epoll_event temp_epoll_event;
	temp_epoll_event.events = EPOLLIN;

	epoll_fd = epoll_create(NUM_CLIENT);
	if (epoll_fd < 0)
		panic("epoll_create error");

	srand(time(0));
	for (int i = 0; i < NUM_CLIENT; i++)
	{
		int port = servers[rand() % servers.size()]->get_port();

		client *c = generate_client(port);
		clients.push_back(c);

		int fd = c->get_fd();

		temp_epoll_event.data.ptr = c;

		epoll_ctl(epoll_fd, EPOLL_CTL_ADD, fd, &temp_epoll_event);

		std::ostringstream *oss = new std::ostringstream();
		*oss << "nick" << fd;
		std::string nick = oss->str();
		c->set_nick(nick);
		delete oss;

		oss = new std::ostringstream();
		*oss << "NICK " << nick << "\n";
		c->add_command(oss->str());
		c->add_command("USER A B C D\n");
		delete oss;
	}
}

client *
generate_client(int port)
{
	int r;
	struct sockaddr_in localhost;
	memset(&localhost, 0, sizeof(localhost));
	r = inet_pton(AF_INET, "127.0.0.1", &localhost);
	localhost.sin_family = AF_INET;
	localhost.sin_port = htons(port);

	int fd = socket(AF_INET, SOCK_STREAM, 0);
	if (fd < 0)
		panic("socket error");

	r = connect(fd, (struct sockaddr *)&localhost, sizeof(localhost));
	if (r < 0)
		panic("connect error");

	client *c = new client(fd);

	return c;
}

void
clear_msg()
{
	for (int i = 0; i < NUM_CLIENT; i++)
	{
		clients[i]->clear_recvmsg();
	}
}

void
change_channel()
{
	channel_to_clients.clear();
	channels.clear();

	std::ostringstream *oss;

	for (int i = 0; i < NUM_CLIENT; i++)
	{
		client *c = clients[i];

		oss = new std::ostringstream();
		int channel = rand() % NUM_CHANNEL;
		*oss << "JOIN #channel" << channel << "\n";
		c->add_command(oss->str());
		delete oss;

		oss = new std::ostringstream();
		*oss << "#channel" << channel;

		channels.push_back(oss->str());
		channel_to_clients.insert(std::pair<std::string, client *>(oss->str(), c));
	}

	clear_msg();
}

