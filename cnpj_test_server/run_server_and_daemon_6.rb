#! /usr/bin/ruby
#
#   2
#  / \
# 1-5-4
#  \ / \
#   3---6
#
#

## SKELETON STOLEN FROM http://www.bigbold.com/snippets/posts/show/1785
require 'socket'
require './rdaemon.rb'
require './irc.rb'
#$SAFE = 1

$total_points = 0
$total_tests = 0

##
# The main program.  Tests are listed below this point.  All tests
# should call the "result" function to report if they pass or fail.
##

def write_config()
    File.open("final_grading1.conf", "w") { |afile|
	afile.puts "1 127.0.0.1 34100 34101 34202"
	afile.puts "2 127.0.0.1 34103 34104 34205"
	afile.puts "3 127.0.0.1 34106 34107 34208"
	afile.puts "5 127.0.0.1 34112 34113 34214"
    }

    File.open("final_grading2.conf", "w") { |afile|
	afile.puts "1 127.0.0.1 34100 34101 34202"
	afile.puts "2 127.0.0.1 34103 34104 34205"
	afile.puts "4 127.0.0.1 34109 34110 34211"
    }

    File.open("final_grading3.conf", "w") { |afile|
	afile.puts "1 127.0.0.1 34100 34101 34202"
	afile.puts "3 127.0.0.1 34106 34107 34208"
	afile.puts "4 127.0.0.1 34109 34110 34211"
	afile.puts "6 127.0.0.1 34115 34116 34217"
    }

    File.open("final_grading4.conf", "w") { |afile|
	afile.puts "4 127.0.0.1 34109 34110 34211"
	afile.puts "2 127.0.0.1 34103 34104 34205"
	afile.puts "3 127.0.0.1 34106 34107 34208"
	afile.puts "5 127.0.0.1 34112 34113 34214"
	afile.puts "6 127.0.0.1 34115 34116 34217"
    }
    
    File.open("final_grading5.conf", "w") { |afile|
	afile.puts "1 127.0.0.1 34100 34101 34202"
	afile.puts "4 127.0.0.1 34109 34110 34211"
	afile.puts "5 127.0.0.1 34112 34113 34214"
    }
    
    File.open("final_grading6.conf", "w") { |afile|
	afile.puts "4 127.0.0.1 34109 34110 34211"
	afile.puts "3 127.0.0.1 34106 34107 34208"
	afile.puts "6 127.0.0.1 34115 34116 34217"
    }
end

# Go through the config file, looking for the "id" passed,
# and spawn it
def spawn_daemon(file,id)
afile = File.open(file, "r")
afile.each_line do |line|
	line.chomp!
	(nodeid, ip, lport, dport, sport)=line.split
	if(nodeid.to_i == id)  # dont forget to convert to int
		puts "./srouted -i #{id} -c #{file} -a 1 &"
	  system("./srouted -i #{id} -c #{file} -a 1 > srouted#{id}.log &")
		return 1 
	end
end
end

def spawn_server(config,id)
afile = File.open(config, "r")
afile.each_line do |line|
	line.chomp!
	(nodeid, ip, lport, dport, sport)=line.split
	if(nodeid.to_i == id)  # dont forget to convert to int
		puts "./sircd #{id} #{config} &> /dev/null &"
		system("./sircd #{id} #{config} > sircd#{id}.log &")
		return 1 
	end
end
end

#### WRITE CONFIG FILE
#
# YOU mAY EDIT THE PORT NUMBERS HERE ONLY
# (to avoid port collisions while testing on andrew)
write_config()

begin

# Spawn the daemons.  This runs command lines of
# ./srouted -i ## -c cprnode##.conf  (where ## == 1 through 4)
spawn_daemon("final_grading4.conf", 4)
spawn_daemon("final_grading5.conf", 5)
spawn_daemon("final_grading6.conf", 6)
spawn_daemon("final_grading1.conf", 1)
spawn_daemon("final_grading2.conf", 2)
spawn_daemon("final_grading3.conf", 3)

puts "Letting the daemons settle."
sleep(2)

spawn_server("final_grading4.conf", 4)
spawn_server("final_grading5.conf", 5)
spawn_server("final_grading6.conf", 6)
spawn_server("final_grading1.conf", 1)
spawn_server("final_grading2.conf", 2)
spawn_server("final_grading3.conf", 3)

# Let the servers settle
puts "Letting the servers settle."
sleep(3000)

rescue Interrupt
rescue Exception => detail
    puts detail.message()
    print detail.backtrace.join("\n")
ensure
#    rdaemon.disconnect()
		system("killall srouted")
		system("killall sircd")
		puts "\nTests passed: #{$total_points} / #{$total_tests}\n"
end


