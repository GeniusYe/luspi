################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/client.cpp \
../src/compare.cpp \
../src/init.cpp \
../src/process.cpp \
../src/receive.cpp \
../src/send.cpp \
../src/test_main.cpp 

OBJS += \
./src/client.o \
./src/compare.o \
./src/init.o \
./src/process.o \
./src/receive.o \
./src/send.o \
./src/test_main.o 

CPP_DEPS += \
./src/client.d \
./src/compare.d \
./src/init.d \
./src/process.d \
./src/receive.d \
./src/send.d \
./src/test_main.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -Ipthread -O3 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


