/*
 * main.cpp
 *
 *  Created on: May 29, 2014
 *      Author: geniusye
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <time.h>
#include <errno.h>
#include <string.h>
#include <setjmp.h>

static jmp_buf jmp_env;

void
error(char *error)
{
	printf("%s errorno : %d\n", error, errno);
	exit(0);
}

static void
handler(int sig, siginfo_t *si, void *uc)
{
	timer_t timerid = *(timer_t *)si->si_value.sival_ptr;
	static int times = 0;
	printf("timer up, sig = %d, timerid = %ld\n", sig, timerid);
	if (times == 3)
	{
		struct itimerspec ts;
		memset(&ts, 0, sizeof(struct itimerspec));
		ts.it_interval.tv_sec = 2;
		ts.it_value.tv_sec = 2;
		timer_settime(timerid, 0, &ts, NULL);
	}
	else if (times == 6)
	{
		timer_delete(timerid);
		longjmp(jmp_env, 1);
	}
	times++;
}

int thread_timer_times = 0;

void threadStart(sigval_t sig)
{
	timer_t timerid = *(timer_t *)sig.sival_ptr;
	printf("timer up, timerid = %ld\n", timerid);
	thread_timer_times++;
}

int main()
{
	int r;

	printf("We are now using signal to measure time\n");

	struct sigaction sa;
	sa.sa_sigaction = handler;
	sa.sa_flags = SA_SIGINFO;
	sigemptyset(&sa.sa_mask);
	r = sigaction(SIGUSR1, &sa, NULL);
	if (r != 0)
		error("sigaction error");

	timer_t tid;
	struct sigevent se;
	se.sigev_notify = SIGEV_SIGNAL;
	se.sigev_signo = SIGUSR1;
	se.sigev_value.sival_ptr = &tid;
	r = timer_create(CLOCK_REALTIME, &se, &tid);
	if (r != 0)
		error("timer create error");

	struct itimerspec ts;
	memset(&ts, 0, sizeof(ts));
	ts.it_interval.tv_sec = 1;
	ts.it_value.tv_sec = 2;
	r = timer_settime(tid, 0, &ts, NULL);
	if (r != 0)
		error("timer settime error");

	if (setjmp(jmp_env) == 0)
	{
		for (;;)
			pause();
	}

	printf("We are now using thread to measure time\n");

	memset(&se, 0, sizeof(se));
	se.sigev_notify = SIGEV_THREAD;
	se.sigev_value.sival_ptr = &tid;
	se.sigev_notify_function = threadStart;
	se.sigev_notify_attributes = NULL;
	r = timer_create(CLOCK_REALTIME, &se, &tid);
	if (r != 0)
		error("timer create error");

	r = timer_settime(tid, 0, &ts, NULL);
	if (r != 0)
		error("timer settime error");

	while (thread_timer_times < 4) { };

	timer_delete(tid);

	return 0;
}


