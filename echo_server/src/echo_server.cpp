//============================================================================
// Name        : echo_server.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/epoll.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>

#define BUFFER_SIZE 2048
#define MAX_EPOLL_EV_LIST 100

using namespace std;

void panic(const std::string &);

char buffer[BUFFER_SIZE];

int main()
{
	int listenfd = socket(AF_INET, SOCK_STREAM, 0);
	if (listenfd < 0)
		panic("listenfd creation failed");

	struct sockaddr_in listenaddr;
	memset(&listenaddr, 0, sizeof(listenaddr));
	listenaddr.sin_family = AF_INET;
	listenaddr.sin_port = htons(8001);
	listenaddr.sin_addr.s_addr = INADDR_ANY;
	int r = bind(listenfd, (struct sockaddr *)&listenaddr, sizeof(listenaddr));
	if (r < 0)
		panic("bind error");

	r = listen(listenfd, 5);
	if (r < 0)
		panic("listen error");

	int epollfd = epoll_create(20);
	if (epollfd < 0)
		panic("epoll_create error");

	struct epoll_event temp_epoll_event;
	temp_epoll_event.events = EPOLLIN | EPOLLRDHUP;
	temp_epoll_event.data.fd = listenfd;

	r = epoll_ctl(epollfd, EPOLL_CTL_ADD, listenfd, &temp_epoll_event);
	if (r < 0)
		panic("epoll_ctl error");

	struct epoll_event evlist[MAX_EPOLL_EV_LIST];

	int count = 1;
	while (true)
	{
		int result = epoll_wait(epollfd, evlist, count, -1);

		std::cout << result << std::endl;

		if (result < 0)
		{
			if (errno == EINTR)
				continue;
			else
				panic("epoll_wait error");
		}
		else
		{
			for (int i = 0; i < result; i++)
			{
				struct epoll_event *curr_ev = &evlist[i];
				if (curr_ev->events & EPOLLIN)
				{
					if (curr_ev->data.fd == listenfd)
					{
						int newfd = accept(listenfd, NULL, NULL);
						temp_epoll_event.data.fd = newfd;
						r = epoll_ctl(epollfd, EPOLL_CTL_ADD, newfd, &temp_epoll_event);
						if (r < 0)
							panic("epoll_ctl_add error");
						else
							std::cout << "accept user with fd " << newfd;
					}
					else if (curr_ev->events & EPOLLRDHUP)
					{
						int curr_fd = curr_ev->data.fd;
						close(curr_fd);
						r = epoll_ctl(epollfd, EPOLL_CTL_DEL, curr_fd, NULL);
						std::cout << "connection closed fd : " << curr_fd << std::endl;
					}
					else
					{
						int curr_fd = curr_ev->data.fd;
						ssize_t len = recv(curr_fd, buffer, BUFFER_SIZE, 0);
						if (len > 0)
						{
							std::cout << len << std::endl;
							std::cout << "receive msg : " << std::string(buffer, len) << std::endl;
							send(curr_fd, buffer, len, 0);
						}
						else
							panic("recv error");
					}
				}
			}
		}
	}

}

void panic(const std::string &error)
{
	std::cout << error << std::endl;
	exit(0);
}
