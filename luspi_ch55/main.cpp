/*
 * main.cpp
 *
 *  Created on: Jun 5, 2014
 *      Author: geniusye
 */

#include <sys/fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <signal.h>

void error(const char *error)
{
	printf("%s\n", error);
	exit(0);
}

int main()
{
	int r;
	int lock_fd = open("ch55.pid", O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);
	if (lock_fd < 0)
		error("open pid file error");

	int flags = fcntl(lock_fd, F_GETFL);
	if (flags < 0)
		error("fcntl get flag error");
	flags |= FD_CLOEXEC;
	r = fcntl(lock_fd, F_SETFL, flags);
	if (r < 0)
		error("fcntl set flag error");

	struct flock fk;
	fk.l_type = F_WRLCK;
	fk.l_whence = SEEK_SET;
	fk.l_start = 0;
	fk.l_len = 0;

	r = fcntl(lock_fd, F_SETLK, &fk);
	if (r < 0)
	{
		if (errno == EACCES || errno == EAGAIN)
		{
			printf("Sorry, we have already run one program\n");
			exit(0);
		}
		else
		{
			error("lock error");
		}
	}

	for (;;);
}





