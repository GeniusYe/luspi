/*
 * main.cpp
 *
 *  Created on: May 21, 2014
 *      Author: geniusye
 */

#include <unistd.h>
#include <stdio.h>
#include <sys/uio.h>
#include <stdlib.h>
#include <string.h>

void error_r(char *err)
{
	printf("%s\n", err);
	exit(0);
}

int main()
{
	int r, fd;
	char tempfile_name[11] = "tempXXXXXX";
	r = mkstemp(tempfile_name);
	if (r < 0)
		error_r("mkstemp error");
	else
		fd = r;

	printf("temp file name : %s\n", tempfile_name);

	int a = 2, c = 3;
	char *b = (char *)malloc(20);

	strcpy(b, "This is old");

	lseek(fd, 10, SEEK_SET);

	struct iovec vec[3];
	vec[0].iov_base = &a;
	vec[0].iov_len = sizeof(a);
	vec[1].iov_base = b;
	vec[1].iov_len = strlen(b);
	vec[2].iov_base = &c;
	vec[2].iov_len = sizeof(c);
	writev(fd, vec, 3);

	*((int *)vec[0].iov_base) = 0;
	strcpy(b, "That is new2");

	printf("Clear the variable: a = %d, b = \"%s\", c = %d\n", a, b, c);

	ssize_t s = preadv(fd, &vec[1], 2, 14);
	printf("%d bytes read\n", s);

	printf("Should be a = 0, b = \"This is old2\", c = 2\n");
	printf("Result is a = %d, b = \"%s\", c = %d\n", a, b, c);

	close(fd);

}




