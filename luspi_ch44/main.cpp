/*
 * main.cpp
 *
 *  Created on: Aug 22, 2014
 *      Author: geniusye
 */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <sys/signal.h>
#include <sys/stat.h>
#include <errno.h>
#include <fcntl.h>

#define MAXLINE 1024

void error(const char *error)
{
	printf("%s\n", error);
	exit(0);
}

void test_pipe()
{
	int pfd[2];
	int r;
	r = pipe(pfd);
	if (r < 0)
		error("pipe error");

	pid_t pid = fork();
	if (pid == 0)
	{
		close(pfd[0]);
		signal(SIGPIPE, SIG_IGN);
		char *buffer = (char *)malloc(MAXLINE);
		while (1)
		{
			printf("Please type to input, type 'quit' to quit\n");
			ssize_t s = read(STDIN_FILENO, buffer, MAXLINE);
			if (s < 0)
				error("read error");

			buffer[s - 1] = '\0';

			if(!strcmp(buffer, "quit"))
			{
				printf("You have typed quit instruction, if you type one more word, this process will be closed\n");
			}

			ssize_t ws = write(pfd[1], buffer, s);
			if (ws < 0 && errno == EPIPE)
			{
				close(pfd[1]);
				printf("Server closed\n");
				break;
			}
		}
		free(buffer);
		exit(0);
	}
	else
	{
		close(pfd[1]);
		char *buffer = (char *)malloc(MAXLINE);
		while (1)
		{
			ssize_t s = read(pfd[0], buffer, MAXLINE);
			if (s < 0)
				error("read error");

			if (!strcmp(buffer, "quit"))
			{
				printf("Receive quit instruction\n");
				close(pfd[0]);
				break;
			}

			printf("Receive data from client, len = %ld, data: %s\n", s, buffer);
		}
		free(buffer);
		wait(NULL);
	}
}

void test_fifo()
{
	char fifo[] = "fifo";

	mkfifo(fifo, S_IRUSR | S_IWUSR | S_IWGRP);

	pid_t pid = fork();
	if (pid == 0)
	{
		int fd = open(fifo, O_WRONLY);
		if (fd < 0)
			error("open write fifo error");

		signal(SIGPIPE, SIG_IGN);
		char *buffer = (char *)malloc(MAXLINE);
		while (1)
		{
			printf("Please type to input, type 'quit' to quit\n");
			ssize_t s = read(STDIN_FILENO, buffer, MAXLINE);
			if (s < 0)
				error("read error");

			buffer[s - 1] = '\0';

			if(!strcmp(buffer, "quit"))
			{
				printf("You have typed quit instruction, if you type one more word, this process will be closed\n");
			}

			ssize_t ws = write(fd, buffer, s);
			if (ws < 0 && errno == EPIPE)
			{
				close(fd);
				printf("Server closed\n");
				break;
			}
		}
		free(buffer);
		exit(0);
	}
	else
	{
		int fd = open(fifo, O_RDONLY);
		if (fd < 0)
			error("open write fifo error");

		char *buffer = (char *)malloc(MAXLINE);
		while (1)
		{
			ssize_t s = read(fd, buffer, MAXLINE);
			if (s < 0)
				error("read error");

			if (!strcmp(buffer, "quit"))
			{
				printf("Receive quit instruction\n");
				close(fd);
				break;
			}

			printf("Receive data from client, len = %ld, data: %s\n", s, buffer);
		}
		free(buffer);
		wait(NULL);
	}
}

int main()
{
	test_fifo();
	printf("fifo successful\n");
	test_pipe();
	printf("pipe successful\n");
}


