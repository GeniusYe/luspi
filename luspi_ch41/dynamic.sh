#!/bin/bash
cc -c -fPIC lib.cpp lib.h
cc -shared -Wl,-soname,libtest.so.1 -o libtest.so lib.o
cp libtest.so /usr/lib/libtest.so.1.0.1
ldconfig
ldconfig -v | grep "libtest"
cc -c main.cpp lib.h 
cc -o prog main.o libtest.so
rm libtest.so

