//============================================================================
// Name        : luspi_ch29.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <vector>
#include <unistd.h>

int count = 0;
pthread_mutex_t lock;
pthread_cond_t cond;

void *threadProduce(void *arg)
{
	pthread_t t = pthread_self();
	for (int i = 0; i < 10; i++)
	{
		pthread_mutex_lock(&lock);
		count++;
		printf("pthread %ld produced one, count = %d\n", t, count);
		pthread_mutex_unlock(&lock);
		pthread_cond_signal(&cond);
		sleep(5);
	}
	return NULL;
}

void *threadConsume(void *arg)
{
	pthread_t t = pthread_self();
	while(true)
	{
		pthread_mutex_lock(&lock);
		while (count == 0)
			pthread_cond_wait(&cond, &lock);
		count--;
		printf("pthread %ld consumed one, count = %d\n", t, count);
		pthread_mutex_unlock(&lock);
	}
	return NULL;
}

int main()
{
	std::vector<pthread_t> pid_list_consume, pid_list_produce;

	pthread_mutex_init(&lock, NULL);
	pthread_cond_init(&cond, NULL);

	for (int i = 0; i < 4; i++)
	{
		pthread_t tid;
		pthread_create(&tid, NULL, threadConsume, NULL);
		pid_list_consume.push_back(tid);
	}

	for (int i = 0; i < 10; i++)
	{
		pthread_t tid;
		pthread_create(&tid, NULL, threadProduce, NULL);
		pid_list_produce.push_back(tid);
	}

	for (int i = 0; i < 10; i++)
	{
		pthread_join(pid_list_produce[i], NULL);
	}

	for (int i = 0; i < 4; i++)
	{
		pthread_cancel(pid_list_consume[i]);
	}
}

void error_report(const char *error)
{
	printf("%s\n", error);
	exit(1);
}
