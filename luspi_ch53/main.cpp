/*
 * main.cpp
 *
 *  Created on: Aug 23, 2014
 *      Author: geniusye
 */

#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>

int error(const char *error)
{
	printf("%s\n", error);
	exit(0);
}

int main(int argc, char **argv)
{
	int r;
	if (argc < 2)
		error("you should ./luspi a");

	//sem will locates on /dev/shm which is /run/shm
	sem_t *sem;
	const char *sem_location = "test";
	sem = sem_open(sem_location, O_CREAT | O_RDWR, S_IRUSR | S_IWUSR, 2);
	if (sem <= 0)
		error("sem open error");

	r = sem_wait(sem);
	if (r < 0)
		error("sem_wait error");

	int curr_v;
	r = sem_getvalue(sem, &curr_v);
	if (r < 0)
		error("sem_getvalue error");

	printf("One process goes in, current_val is %d, id is %s\n", curr_v, argv[1]);

	sleep(20);

	sem_post(sem);

	sem_close(sem);

}




