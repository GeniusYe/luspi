/*
 * main.cpp
 *
 * This program mainly concerns about unix socket
 *
 *  Created on: Aug 19, 2014
 *      Author: geniusye
 */

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <stdlib.h>
#include <errno.h>

#define MAXLINE 1024

void error(const char *error)
{
	printf("%s\n", error);
	exit(0);
}

void client_fun(int i);
void server_fun(int i);

char unix_path[] = "/tmp/af_unix";

int main(int argc, char **argv)
{
	remove("/tmp/af_unix");

	int r;

	if (argc <= 1)
		error ("You need to provide parameter");

	if (!strcmp(argv[1], "-b"))
	{
		int sockfd[2];
		r = socketpair(AF_UNIX, SOCK_STREAM, 0, sockfd);
		if (r < 0)
			error("socketpair error");

		pid_t p = fork();

		if (p == 0)
			client_fun(sockfd[0]);
		else
			server_fun(sockfd[1]);
	}
	else if (!strcmp(argv[1], "-c"))
	{
		int fd = socket(AF_UNIX, SOCK_DGRAM, 0);
		if (fd < 0)
			error("socket error");

		struct sockaddr_un address;
		memset(&address, 0, sizeof(struct sockaddr_un));
		address.sun_family = AF_UNIX;
		strcpy(address.sun_path, "/tmp/client_sock");

		r = bind(fd, (struct sockaddr *)&address, sizeof(struct sockaddr_un));
		if (r != 0)
			error("bind error");

		client_fun(fd);
	}
	else if (!strcmp(argv[1], "-s"))
	{
		int fd = socket(AF_UNIX, SOCK_DGRAM, 0);
		if (fd < 0)
			error("socket error");

		struct sockaddr_un address;
		memset(&address, 0, sizeof(struct sockaddr_un));
		address.sun_family = AF_UNIX;
		strcpy(address.sun_path, unix_path);

		r = bind(fd, (struct sockaddr *)&address, sizeof(struct sockaddr_un));
		if (r != 0)
			error("bind error");

		server_fun(fd);
	}
	else
		error("You need to provide parameter -b => perform by one process -c client -s server");
	return 0;
}

void server_fun(int fd)
{
	char *buffer = (char *)malloc(MAXLINE);

	struct sockaddr_un addr;

	while (1)
	{
		int s = recvfrom(fd, buffer, MAXLINE, 0, (struct sockaddr *)&addr, NULL);
		if (s < 0)
		{
			printf("%d\n", errno);
			error("recv error");
		}
		else if (s == 0)
			break;

		printf("Server received data from client, len = %d\nData:%s\n", (int)s, buffer);
	}
	free(buffer);
}

void client_fun(int fd)
{
	char *buffer = (char *)malloc(MAXLINE);

	struct sockaddr_un address;
	memset(&address, 0, sizeof(struct sockaddr_un));
	address.sun_family = AF_UNIX;
	strcpy(address.sun_path, unix_path);

	while (1)
	{
		printf("Type in command, type in quit to quit\n");
		scanf("%s", buffer);
		if (!strcmp(buffer, "quit"))
			break;

		int len = strlen(buffer);
		char *start = buffer;

		do
		{
			int s = sendto(fd, start, len, 0, (struct sockaddr *)&address, sizeof(struct sockaddr_un));
			if (s < 0)
			{
				printf("%d\n", errno);
				error("send error");
			}
			else if (s < len)
			{
				start += s;
				len -= s;
			}
			else
				break;
		}
		while (1);
	}
	free(buffer);
}


