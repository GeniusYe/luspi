/*
 * main.cpp
 *
 *  Created on: Jun 5, 2014
 *      Author: geniusye
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <syslog.h>

static char *ident = "luspi_ch37";

void error(const char *error)
{
	printf("%s\n", error);
	exit(0);
}

static void sighup_handler(int sig)
{
	printf("SIGHUP_HANDLER executed\n");
	syslog(LOG_NOTICE, "SIGHUP received");
}

int main()
{
	int r;

	switch (fork())
	{
	case -1:
		error("fork error");
		break;
	case 0:
		break;
	default:
		_exit(EXIT_SUCCESS);
	}

	//umask(0);

	r = setsid();
	if (r < 0)
		error("setsid error");

	chdir("/");

	close(STDIN_FILENO);

	int fd = open("/dev/null", O_RDWR);
	if (fd != STDIN_FILENO)
		error("fd should be 0");

	printf("Please use \"killall -HUP luspi_ch37\" to test\n");

	dup2(STDIN_FILENO, STDOUT_FILENO);
	dup2(STDIN_FILENO, STDERR_FILENO);

	openlog(ident, LOG_PID | LOG_CONS | LOG_NDELAY, LOG_LOCAL0);

	syslog(LOG_INFO, "start daemon");

	struct sigaction sa;
	sa.sa_handler = sighup_handler;
	sa.sa_flags = SA_RESTART;
	sigemptyset(&sa.sa_mask);
	r = sigaction(SIGHUP, &sa, NULL);
	if (r < 0)
		error("sigaction error");

	for (;;) pause();

	closelog();
}



