/*
 * main.cpp
 *
 *  Created on: Aug 23, 2014
 *      Author: geniusye
 */

#include <fcntl.h>
#include <sys/mman.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

int error(const char *error)
{
	printf("%s\n", error);
	exit(0);
}

int main(int argc, char **argv)
{
	if (argc < 2)
	{
		printf("You should use ./exe -c or -w or -p, you should run ./exe -c to create, run ./exe -w to write to shared memory and then run ./exe -p to read them\n");
		return 0;
	}
	int len = 1000;
	int r;
	const char *shm_name = "shm_test";

	int fd;

	if (!strcmp(argv[1], "-c"))
	{
		fd = shm_open(shm_name, O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);
		if (fd < 0)
			error("shm open error");

		r = ftruncate(fd, len);
		if (r < 0)
			error("truncate error");

		close(fd);
	}
	else if (!strcmp(argv[1], "-w"))
	{
		fd = shm_open(shm_name, O_RDWR, 0);
		if (fd < 0)
			error("shm open error");

		void *map = mmap(NULL, len, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
		if (map == MAP_FAILED)
			error("mmap error");

		close(fd);

		char *new_map = (char *)map;
		for (int i = 0; i < len; i++)
		{
			*new_map = 'a' + i % 26;
			new_map++;
			sleep(1);
		}
	}
	else
	{
		fd = shm_open(shm_name, O_RDWR, 0);
		if (fd < 0)
			error("shm open error");

		void *map = mmap(NULL, len, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
		if (map == MAP_FAILED)
			error("mmap error");

		close(fd);

		printf("map position : %p\n", map);

		char *new_map = (char *)map;
		for (int i = 0; i < len; i++)
		{
			printf("%c\t", *new_map);
			fflush(stdout);
			new_map++;
			sleep(1);
		}
	}
}

