/*
 * main.cpp
 *
 *  Created on: Aug 26, 2014
 *      Author: geniusye
 */

#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>

void error(const char *error)
{
	printf("%s\n", error);
	exit(0);
}

int main()
{
	void *handle = dlopen("/usr/lib/libtest.so", RTLD_LAZY);
	if (handle == NULL)
		error(dlerror());

	void (*fun)();

	*(void **)&fun = dlsym(handle, "lib_fun");
	printf(dlerror());
	printf("Fun locates at %p\n", fun);
	int *r = (int *)dlsym(handle, "r");
	printf("R locats at %p, r = %d\n", r, *r);
	fflush(stdout);

	//int r = fun(NULL, NULL, threadFun, 0);

	printf("In main\n");

	for(;;);

}


