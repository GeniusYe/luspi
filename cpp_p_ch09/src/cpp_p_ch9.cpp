//============================================================================
// Name        : cpp_p_ch9.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <vector>
#include <algorithm>
#include <functional>

template<typename It> void print (std::string name, It b, It e);

bool check (const int i, const int compare);

int main() {
	std::cout << "Vector" << std::endl;
	std::vector<int> vec = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
	print("vec:", vec.begin(), vec.end());
	std::vector<int> vec2 = { 10, 20 };
	print("vec2:", vec2.begin(), vec2.end());
	copy_if(vec.begin(), vec.end(), back_inserter(vec2), [] (const int i) { if (i > 5) return true; else return false; } );
	print("After copy:", vec2.begin(), vec2.end());
	int low_b = 7, upp_b = 9, replace_to = 30;
	replace_if(vec2.begin(), vec2.end(), [low_b, upp_b] (const int i) { if (i >= low_b && i <= upp_b) return true; else return false; }, replace_to );
	print("After replace:", vec2.begin(), vec2.end());
	fill_n(vec2.begin() + 2, 2, 3);
	print("After fill:", vec2.begin(), vec2.end());
	print("vec:", vec.begin(), vec.end());
	print("vec2:", vec2.begin(), vec2.end());
	swap(vec, vec2);
	std::cout << "After Swap" << std::endl;
	print("vec:", vec.begin(), vec.end());
	print("vec2:", vec2.begin(), vec2.end());
	sort(vec.begin(), vec.end());
	print("After sort:", vec.begin(), vec.end());
	std::vector<int>::iterator iter = unique(vec.begin(), vec.end());
	print("After unique:", vec.begin(), vec.end());
	vec.erase(iter, vec.end());
	print("After unique and delete:", vec.begin(), vec.end());
	std::cout << "Accumulate:" << accumulate(vec.begin(), vec.end(), 0) << std::endl;
	copy(vec2.begin(), vec2.end(), inserter(vec, vec.begin() + 3));
	print("After insert vec2 into vec+3:", vec.begin(), vec.end());
	int compare_no = 6;
	std::cout << "Find the number > " << compare_no << std::endl;
	iter = vec.begin();
	while (true)
	{
		iter = find_if(iter, vec.end(), bind(&check, std::placeholders::_1, compare_no));
		if (iter == vec.end())
			break;
		std::cout << *iter << " ";
		++iter;
	}
	std::cout << std::endl;

	std::cout << "Forward_List" << std::endl;

	return 0;
}

bool check (const int i, const int compare)
{
	if (i > compare)
		return true;
	else
		return false;
}

template<typename It> void print (std::string name, It b, It e)
{
	std::cout << name << std::endl;
	typedef decltype(*b) T;
	for_each (b, e, [] (const T i) { std::cout << i << " "; });
//	for (; b != e; ++b)
//		std::cout << *b << " ";
	std::cout << std::endl;
}
