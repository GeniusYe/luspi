/*
 * main.cpp
 *
 *  Created on: Aug 22, 2014
 *      Author: geniusye
 */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <sys/signal.h>
#include <errno.h>
#include <fcntl.h>
#include <mqueue.h>

#define MAXLINE 10240

void error(const char *error)
{
	printf("%s\n", error);
	exit(0);
}

void handler(int sig, siginfo_t *siginfo, void *)
{
	mqd_t mqd = siginfo->si_value.sival_int;
	printf("mqd = %d\n", mqd);
}

void test_posix_ipc()
{
	char fifo[] = "/test_ipc";

	struct mq_attr attr;
	memset(&attr, 0, sizeof(struct mq_attr));
	attr.mq_msgsize = 1000;

	mqd_t mqd = mq_open(fifo, O_CREAT, S_IRUSR | S_IWUSR);
	if (mqd < 0)
		error("open write fifo error");
	mq_setattr(mqd, &attr, NULL);
	mq_close(mqd);

	pid_t pid = fork();
	if (pid == 0)
	{
		mqd_t mqd = mq_open(fifo, O_CREAT | O_WRONLY, S_IRUSR | S_IWUSR);
		if (mqd < 0)
			error("open write fifo error");
		mq_setattr(mqd, &attr, NULL);

		signal(SIGPIPE, SIG_IGN);
		char *buffer = (char *)malloc(MAXLINE);
		while (1)
		{
			printf("Please type to input, type 'quit' to quit\n");
			ssize_t s = read(STDIN_FILENO, buffer, MAXLINE);
			if (s < 0)
				error("read error");

			buffer[s - 1] = '\0';

			ssize_t ws = mq_send(mqd, buffer, s, 1);

			if(!strcmp(buffer, "quit"))
			{
				mq_unlink(fifo);
				mq_close(mqd);
				break;
			}
		}
		free(buffer);
		exit(0);
	}
	else
	{
		mqd_t mqd = mq_open(fifo, O_CREAT | O_RDONLY, S_IRUSR | S_IWUSR);
		if (mqd < 0)
			error("open write fifo error");
		mq_setattr(mqd, &attr, NULL);

		char *buffer = (char *)malloc(MAXLINE);
		unsigned int pri;
		while (1)
		{
			ssize_t s = mq_receive(mqd, buffer, MAXLINE, &pri);
			if (s < 0)
			{
				printf("receive error errno:%d\n", errno);
			}

			printf("Receive data from client, len = %ld, pri = %d, data: %s\n", s, pri, buffer);

			if (!strcmp(buffer, "quit"))
			{
				printf("Receive quit instruction\n");
				mq_close(mqd);
				break;
			}
			else if (!strcmp(buffer, "signal"))
			{
				int r;
				struct sigaction sa;
				sa.sa_flags = 0;
				sigemptyset(&sa.sa_mask);
				sa.sa_sigaction = handler;
				r = sigaction(SIGUSR1, &sa, NULL);
				if (r < 0)
					error("sigaction error");

				sigset_t ss;
				sigemptyset(&ss);
				sigaddset(&ss, SIGUSR1);
				r = sigprocmask(SIG_BLOCK, &ss, NULL);

				struct sigevent se;
				se.sigev_notify = SIGEV_SIGNAL;
				se.sigev_signo = SIGUSR1;
				se.sigev_value.sival_int = mqd;
				r = mq_notify(mqd, &se);
				if (r < 0)
					error("mq notify error");

				printf("waiting data\n");
				sigemptyset(&ss);
				sigsuspend(&ss);
			}
		}
		free(buffer);
		wait(NULL);
	}
}

int main()
{
	test_posix_ipc();
	printf("ipc successful\n");
}


