/*
 * main.cpp
 *
 *  Created on: May 25, 2014
 *      Author: geniusye
 */

#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <string.h>

/**
 *
 * wait for SIGINT (press Ctrl+C three times), handler are called twice, and is not recursly called, but lineared called.
 * ^Cin handler of sigint before sleep
 * ^C^Cin handler of sigint after sleep
 * in handler of sigint before sleep
 * in handler of sigint after sleep
 * interrupted by SIGINT
 */

pid_t pid;

void
sigint_handler(int sig)
{
	printf("in handler of sigint before sleep\n");
	sleep(1);
	printf("in handler of sigint after sleep\n");
}

void
kill_handler(int sig)
{
	printf("in kill handler\n");
	kill(pid, SIGABRT);
}

void
sigint_handler2(int sig)
{
	static int state = 0;
	printf("in sigint 2\n");
	if (state == 0)
		state = 1;
	else
	{
		signal(sig, SIG_DFL);
		raise(sig);
	}
}

int sigint_handler3_status = 0;

void
sigint_handler3(int sig)
{
	static int state = 0;
	printf("in sigint 3\n");
	if (state < 2)
		state++;
	else
	{
		sigint_handler3_status = 1;
	}
}

void
sigint_handler4(int sig)
{
	printf("%s\n", strsignal(sig));
}

void
error(char *error)
{
	printf("%s\n", error);
	exit(0);
}

int main()
{
	struct sigaction sa;
	int r;

	//Test SIGINT

	signal(SIGINT, sigint_handler);

	printf("Wait for SIGINT\n");

	pause();

	printf("Interrupted by SIGINT\n");

	//Test SIG_IGN

	signal(SIGINT, SIG_IGN);

	//Do not pause, since sig_ign would not break pause out
	//pause();

	//Test kill

	pid = fork();
	if (pid != 0)
	{
		signal(SIGINT, kill_handler);

		printf("Try kill, please type Ctrl+C\n");

		int status;
		pid_t recv_pid = waitpid(-1, &status, NULL);

		printf("pid = %d, ifexited = %d, ifsignaled = %d\n", recv_pid, WIFEXITED(status), WIFSIGNALED(status));
	}
	else
		while(1);

	//Try multi process signal

	pid = fork();
	if (pid == 0)
	{
		struct sigaction sa;
		memset(&sa, 0, sizeof(sa));
		sa.sa_handler = sigint_handler2;
		int r = sigaction(SIGINT, &sa, NULL);
		if (r < 0)
			error("sigaction error\n");
		printf("in child process\n");
	}
	else
	{
		struct sigaction sa;
		memset(&sa, 0, sizeof(sa));
		sa.sa_handler = sigint_handler3;
		int r = sigaction(SIGINT, &sa, NULL);
		if (r < 0)
			error("sigaction error\n");
		printf("in parent process\n");
	}

	printf("Try multi process receive signal, please type Ctrl+C 3 times\n");

	while (sigint_handler3_status == 0)
		pause();

	printf("Interrupted by 3 times of SIGINT\n");

	//Test recursively SIGINT

	memset(&sa, 0, sizeof(sa));
	sa.sa_handler = sigint_handler;
	sa.sa_flags = SA_NODEFER;
	r = sigaction(SIGINT, &sa, NULL);
	if (r < 0)
		error("sigaction error\n");

	printf("Try recursively called SIGINT, please type Ctrl+C twice \n");

	pause();

	printf("Interrupted by SIGINT\n");

	//Test signal mask

	sigset_t s;
	sigemptyset(&s);
	sigaddset(&s, SIGTSTP);
	sigprocmask(SIG_BLOCK, &s, NULL);

	memset(&sa, 0, sizeof(sa));
	sa.sa_handler = sigint_handler4;
	r = sigaction(SIGINT, &sa, NULL);
	if (r < 0)
		error("sigaction error\n");
	r = sigaction(SIGTSTP, &sa, NULL);
	if (r < 0)
		error("sigaction error\n");

	printf("Try block out signal SIGTSTP, please type Ctrl+Z, and use Ctrl+C to continue\n");

	pause();

	printf("\"Stop\" words showed here when we unblock the signal\n");

	sigprocmask(SIG_UNBLOCK, &s, NULL);

	printf("Interrupted by SIGINT\n");

	//Test auto block mask

	memset(&sa, 0, sizeof(sa));
	sa.sa_handler = sigint_handler4;
	r = sigaction(SIGTSTP, &sa, NULL);
	if (r < 0)
		error("sigaction error\n");

	memset(&sa, 0, sizeof(sa));
	sa.sa_handler = sigint_handler;
	sa.sa_flags = 0;
	r = sigaction(SIGINT, &sa, NULL);
	if (r < 0)
		error("sigaction error\n");

	printf("Try automanticly block out signal SIGTSTP, please try Ctrl+C and immediately type Ctrl+Z\nThis time not blocked, SIGINT handler should be interrupted by SIGTSTP\n");

	pause();

	memset(&sa, 0, sizeof(sa));
	sa.sa_handler = sigint_handler;
	sa.sa_mask = s;
	sa.sa_flags = 0;
	r = sigaction(SIGINT, &sa, NULL);
	if (r < 0)
		error("sigaction error\n");

	printf("Try automanticly block out signal SIGTSTP, please try Ctrl+C and immediately type Ctrl+Z\nThis time blocked, SIGINT handler should not be interrupted by SIGTSTP\n");

	pause();

	//Test sigsuspend

	sigset_t ss;
	sigfillset(&ss);
	sigdelset(&ss, SIGTSTP);

	printf("Try sigsuspend, we do not use pause, try Ctrl+Z to see the result\n");

	sigsuspend(&ss);

	printf("All success, about to exit\n");

	return 0;
}


